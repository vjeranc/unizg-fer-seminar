\documentclass[times, utf8, seminar, numeric]{fer}
\usepackage{booktabs}
\usepackage{url}
\usepackage[unicode]{hyperref} % make seminar clickable
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{subcaption}
\usepackage{enumitem}
\usepackage{float}
\usepackage{pgffor}


\begin{document}

% Ukljuci literaturu u seminar

% TODO: Navedite naslov rada.
\title{Analiza sentimenta i mišljenja korištenjem vjerojatnosnih grafičkih modela}

% TODO: Navedite vaše ime i prezime.
\author{Vjeran Crnjak}

% TODO: Navedite ime i prezime voditelja.
\voditelj{dr.~sc.~Jan Šnajder}

\maketitle

\tableofcontents

\nocite{*}

\chapter{Uvod}
U ovom seminarskom radu opisani su postupci analiziranja sentimenta i mišljenja korisnika u korisnički generiranom sadržaju na Internetu. Blogovi, forumi i slične web stranice su lokacije s izobiljem nestrukturiranog teksta koji sigurno sadrži informacije o tome kakvo je mišljenje određene populacije o nekoj temi, osobi, proizvodu i sl. Interesa za prepoznavanje mišljenja i stavova ima, a u ovom radu naglasak je na metode koje koriste vjerojatnosne grafičke modele (engl.~\emph{probabilistic graphical models}).

\citet{liu2012sentiment} opisuje analizu sentimenta i mišljenja kao područje istraživanja koje analizira mišljenja, procjene, stavove i osjećaje ljudi iz pisanog jezika. Jedno je od najaktivnijih istraživačkih područja u obradi prirodnog jezika i široko ga proučavaju u dubinskoj analizi podataka, weba i teksta. Istraživanje se proširilo izvan računarske znanosti na menadžment i ostale društvene znanosti zbog svoje važnosti u poslovanju i društvu u cijelosti. Rast važnosti analize sentimenta podudara s rastom društvenih medija na kojima ljudi mogu ostavljati recenzije, sudjelovati u forumskim diskusijama, pisati svoje blogove i mikro blogove, sudjelovati na medijima poput Twittera i ostalih društvenih mreža. Prvi put u povijesti čovječanstva posjedujemo ogromnu količinu podataka koji sadrže mišljenje u digitalnom obliku prikladnom za analizu potpomognutu računalom.

\citet{koller2009probabilistic} opisuju vjerojatnosne grafičke modele kao kompaktnu reprezentaciju složene distribucije -- u obliku grafa -- gdje graf opisuje sve međusobne zavisnosti između slučajnih varijabli. Naglašavaju da su tri ključne komponente za izgradnju inteligentnih sustava -- reprezentacija, zaključivanje i učenje -- obuhvaćene u okviru vjerojatnosnih grafičkih modela te je zbog toga pomoću njih moguće rješavati široki spektar problema. Zbog svoje kompaktnosti, efikasnosti i ekspresivnosti vjerojatnosni grafički modeli jedan su od glavnih pristupa u analizi sentimenta i mišljenja.

\chapter{Uvod u analizu sentimenta i mišljenja}

Tekst koji slijedi sažetak je teksta autora \citet{pang2008opinion, liu2012sentiment} iz uvodnih poglavlja -- uz sadržaj nekih radova koje su oni citirali.

\section{Uvod}

Analiza sentimenta (engl.~\emph{sentiment analysis}), ili analiza mišljenja (engl.~\emph{opinion mining}), je područje znanosti koje analizira mišljenje, osjećaje, stavove, odobravanja ljudi i slično prema entitetima kao što su proizvodi, usluge, organizacije, pojedinci, opći problemi, događaji, teme, te prema atributima tih entiteta. Postoji mnoštvo potproblema u području analize sentimenta. Mnoštvo je naziva prisutno i različitih zadataka, npr.~, \emph{analiza sentimenta}, \emph{ mišljenja}, \emph{ekstrakcija mišljenja} (engl.~\emph{opinion extraction}), \emph{dubinska analiza sentimenta} (engl.~\emph{sentiment mining}), \emph{analiza subjektivnosti} (engl.~\emph{subjectivity analysis}), \emph{analiza afekta} (engl.~\emph{affect analysis}), \emph{analiza emocija} (engl.~\emph{emotion analysis}), \emph{dubinska analiza recenzija} (engl.~\emph{review mining}) i sl. Svi su ti podzadaci pod okriljem analize sentimenta ili mišljenja. U industriji češće se koristi prvi izraz, dok su u akademiji oba jednoliko upotrebljena \cite{liu2012sentiment}. U ovom će se seminaru svi navedeni termini odnositi na istu stvar.

Unatoč tome što računalna lingvistika i obrada prirodnog jezika imaju bogatu povijest, malo je istraživanja na temu mišljenja ljudi i sentimenta prije 2000.~godine. Od tada je to područje istraživanja postalo vrlo aktivno. Postoji više razloga za to:
\begin{itemize}
\item Postoji širok opseg primjena.
\item Postoji još mnoštvo izazovnih istraživačkih problema koje nitko prije nije proučavao.
\item Po prvi puta u povijesti čovječanstva prisutna je velika količina subjektivnih podataka -- na društvenim medijima i na Internetu. Bez tih podataka istraživanje nekih problema ne bi bilo moguće. Uz javno dostupne podatke izgrađeni su i skupovi za učenje nad kojima se mogu koristiti algoritmi strojnog učenja. Pojava velikog broja stranica koje agregiraju recenzije i stavljaju te podatke javno je uvelike olakšala prikupljanje i eksperimentiranje.
\item Veliki napredak u metodama strojnog učenja, obrade prirodnog jezika i pretraživanja informacija (engl.~\emph{information retrieval}).

\end{itemize}

\section{Područja primjene}

\subsection{Motivacija}

U skoro svim ljudskim aktivnostima mišljenja imaju najveći utjecaj na naše ponašanje. Kad god želimo nešto poduzeti nerijetko se odlučujemo znati što drugi misle o tome. U stvarnom svijetu, firme i organizacije uvijek žele znati mišljenje svojih potrošača ili javnosti o njihovim proizvodima i uslugama. Pojedinačni potrošači žele znati mišljenje postojećih korisnika nekog proizvoda prije nego što ga odluče kupiti, ili tuđe mišljenje o političkim kandidatima prije nego odluče za koga će glasati. Prije su ljudi tražili mišljenje svojih prijatelja i obitelji. Kada su organizacije i firme tražile javno mišljenje ili mišljenje potrošača, radili su ankete, upitnike, ili fokusne grupe. Prikupljanje javnog mišljenja je dugo bio unosan posao za marketing, odnose s javnošću i poduzeća koja su se bavila političkim kampanjama.

Eksplozivan rast informacija (npr.~recenzije, rasprave na forumima, blogovi, mikroblogovi, Twitter, komentari na portalima, i objave na društvenim mrežama) na Internetu omogućio je pojedincima i organizacijama korištenje tog sadržaja za donošenje odluka. Danas, ako netko želi kupiti neki proizvod više nije ograničen mišljenjima svojih prijatelja i obitelji jer postoji mnoštvo korisničkih recenzija i rasprava na javnim forumima na Internetu o proizvodima. Za organizaciju vjerojatno više nije potrebno održavati ankete, upitnike, fokusne grupe kako bi se prikupilo javno mišljenje jer je takva informacija pristupna javno u izobilju. Unatoč tome pronalaženje i praćenje takvih mjesta na Internetu i sažimanje/filtriranje informacije je zahtjevan zadatak. Prosječni čitač takvih stranica imat će poteškoća s identificiranjem i sažimanjem relevantnih informacija. Zbog toga su potrebni automatizirani sustavi za analizu sentimenta.

Danas postoji mnoštvo firmi koje se upravo bave navedenom problematikom. Posao je očito unosan i izazovan.

\subsection{Primjena na web-stranice koje agregiraju recenzije}

Očito je da primjena sustava za analizu sentimenta na stranice koje imaju ogroman sadržaj u svojoj bazi recenzija otvara nove mogućnosti prikaza i korištenja tih podataka.

Sažimanje korisničkih recenzija je vrlo važan problem. Također, moguće je da će se i greške u korisničkim ocjenama lako ispravljati. Sigurno postoje slučajevi gdje korisnik slučajno odabire nisku ocjenu bez obzira na to što je njegova tekstna recenzija pozitivna. Postoje i pokazatelji da korisnici znaju biti jako pristrani kada ocjenjuju i njihove bi ocjene trebale biti korigirane, a automatski klasifikatori bi mogli riješiti takav problem.

\subsection{Primjena kao dio podsustava}

Sustavi za analizu sentimenta i mišljenja jedni su od glavnih komponenata u drugim tehnološkim rješenjima.

Jedna od mogućnosti je prilagoditi sustave za preporuke (engl.~\emph{recommendation systems}) tako da ne preporučuju proizvode koji imaju puno negativnih recenzija (pretpostavka je da ocjena proizvoda nije prisutna).

Prepoznavanje strastvenih osjećaja poput mržnje u elektronskoj pošti ili drugim tipovima komunikacije je još jedna moguća primjena prepoznavanja subjektivnosti ili klasificiranja iste.

Prepoznavanje stranica na kojima je prisutan osjetljiv sadržaj neprikladan za oglase bi uvelike povećalo kvalitetu sustava koji služe za internetsko oglašavanje. Za sofisticiranije sustave vjerojatno bi bilo korisno prikazati oglase za proizvode kada je za njih prisutan relevantan pozitivni sentiment, ili spriječiti prikaz kada je prepoznat relevantan negativan sentiment.

Postoji i rasprava o tome da bi izvlačenje informacija moglo biti poboljšano odbacivanjem onih informacija koje su prisutne u subjektivnim rečenicama \cite{riloff2005exploiting}.

Odgovaranje na pitanja (engl.~\emph{question answering}) je još jedno područje gdje je analiza sentimenta korisna. Naprimjer, pitanja koja sadrže mišljenje bi vjerojatno zahtijevala drugačiji tretman. Recimo davanje odgovora koji sadrži više informacije o tome kakav je stav prema nekom entitetu bi vjerojatno bolje informiralo korisnika.

Sažimanje teksta (engl.~\emph{summarization}) bi moglo biti poboljšano uzimanjem u obzir mišljenja s više stajališta.

Analiza citata (engl.~\emph{citation analysis}), npr.~, možda bi bilo zgodno vidjeti je li autor citira rad kao dokaz koji potkrjepljuje njegove tvrdnje ili autor taj rad odbacuje.

\subsection{Primjena u poslovnoj inteligenciji}

Uzmimo, naprimjer, sljedeći scenarij. Veliki proizvođač računala razočaran zbog neočekivano malenog broja prodanih proizvoda postavlja si sljedeće pitanje: "Zašto potrošači ne kupuju naša računala?" Koliko god su konkretni podaci o konfiguraciji računala i cijenama konkurentnih proizvođača bitni, odgovor na ovo pitanje se nalazi u subjektivnim stajalištima ljudi o tim objektivnim, konkretnim podacima. Također, subjektivna stajališta o teško definiranim kvalitetama proizvoda kao što je dizajn i sl.~ trebaju biti uzeta u obzir.

Bilo bi jako teško napraviti anketu kupaca računala koji nisu kupili proizvod tog proizvođača. Stoga primjena automatizirane analize sentimenta i mišljenja je ovdje prijeko potrebna.

\subsection{Primjena u drugim područjima}

Sve primjene do sad su uglavnom bile prisutne u industrijskim područjima koje su prožete računarstvom. Mišljenja puno znače u politici. Postoji mnoštvo radova u kojima se pokušava automatizirano razumjeti glasače dok drugi projekti pokušavaju sažeti stajališta raznih političara kako bi se povećala dostupnost kvalitetnih informacija za glasače.

Analiza sentimenta predložena je kao glavni tehnološki korak k internetskom stvaranju novih zakona i unapređenju pravne strukture države (engl.~\emph{eRulemaking}). Omogućila bi automatsku analizu mišljenja ljudi koji ostavljaju komentare ili daju nove prijedloge.

\section{Definicije i pojmovi}

U nastavku slijede definicije i pojmovi koji su potrebni za praćenje ovog teksta vezani uz analizu sentimenta. Čitatelja se upućuje na \cite{liu2012sentiment, pang2008opinion, shanahan2006computing} gdje su česti termini, definicije, problematika i najsuvremenije metode opisane i povezane (najnovija referenca) jer postoji mogućnost da se nešto izostavilo zbog nevoljkosti za ponavljanjem.

\subsection{Razine granularnosti analize sentimenta}\label{razinegranularnosti}

Analiza sentimenta može se vršiti kroz više razina. U općem slučaju, analiza sentimenta se uglavnom istraživala na tri razine.

\begin{itemize}
\item \textbf{Razina dokumenta:} Zadatak na ovoj razini je klasificirati je li cijelo mišljenje dokumenta izražava pozitivni ili negativni sentiment \cite{pang2002thumbs}. Ovaj zadatak se u literaturi naziva "sentiment klasifikacija na razini dokumenta" (engl.~\emph{document-level sentiment classification}). Analiza na ovoj razini pretpostavlja da svaki dokument izražava mišljenje o samo jednom entitetu (npr.~o samo jednom proizvodu) stoga ju nije moguće primjenjivati na dokumentima koji uspoređuju ili evaluiraju više entiteta istovremeno.
\item \textbf{Razina rečenice:} Zadatak na ovoj razini je klasificirati je li svaka pojedina rečenica izražava pozitivno, negativno, ili neutralno mišljenje. Ova razina analize je usko povezana s klasifikacijom subjektivnosti \cite{wiebe1999development} gdje se razlikuju rečenice zvane \emph{objektivne rečenice} od onih u kojima postoji subjektivno mišljenje. Naravno, subjektivnost nije ekvivalent sentimentu.
\item \textbf{Entitetska i aspektna razina:} Analiza na razini dokumenta i rečenica ne otkriva što su to točno ljudi voljeli, a što  nisu, što odobravaju, a što ne. Aspektna razina (engl.~\emph{aspect level}) direktno proučava mišljenje. Bazira se na ideji da se mišljenje sastoji od sentimenta (pozitivnog ili negativnog) i predmeta promatranja (predmeta mišljenja). Identificiramo li mišljenje bez da znamo na što se odnosi to nam nije od prevelike koristi.
\end{itemize}

\subsection{Nedostaci metoda obrade prirodnog jezika}

Ne smijemo zaboraviti da je analiza sentimenta problem obrade prirodnog jezika. Dodiruje mnoštvo problema kao što su razrješavanje koreference (engl.~\emph{coreference resolution}), obrada negacije, i otkrivanje značenja riječi (engl.~\emph{word sense disambiguation}), a svaki od ovih problema otežava problem određivanja sentimenta jer ni jedan nije u potpunosti riješen.

\chapter{Uvod u vjerojatnosne grafičke modele}

\section{Uvod}

\citet{wainwright2008graphical} opisuju grafičke modele kao povezivanje teorije grafova i teorije vjerojatnosti u moćan matematički formalizam multivarijatnog statističkog modeliranja. U raznim primijenjenim disciplinama poput bioinformatike, prepoznavanja govora, obradi slike i teorije kontrole, statistički modeli su često formulirani pomoću grafova, i algoritmi za računanje osnovnih statističkih veličina -- kao što je vjerodostojnost -- su često izraženi oslanjajući se na te grafove. Primjeri modela su skriveni Markovljevi modeli, Markovljeva slučajna polja, Kalmanov filtar i sl. Sve te ideje mogu se unificirati i generalizirati unutar formalizma grafičkih modela.

Vjerojatnosni grafički modeli koriste reprezentaciju grafom kao temelj za enkodiranje potpune distribucije vjerojatnosti višedimenzionalnog prostora, a taj graf je kompaktan (ili faktoriziran) prikaz skupa neovisnosti (ili ovisnosti) koje vrijede u određenoj vjerojatnosnoj distribuciji. Dvije korištene grane grafičkih reprezentacija vjerojatnosnih distribucija su Bayesove i Markovljeve mreže (\ref{bayesmarkovnetfig}). Obje familije obuhvaćaju svojstva faktorizacije i neovisnosti određene vjerojatnosne distribucije, ali se razlikuju u skupu neovisnosti koje mogu enkodirati i faktorizaciji vjerojatnosne distribucije koju mogu inducirati \cite{koller2009probabilistic}.

\begin{figure}[H]
\tikzstyle{lightedge}=[<-,dotted]
\tikzstyle{mainstate}=[state,thick]
\tikzstyle{mainedge}=[<-,thick]
\tikzstyle{undirect}=[draw=black,fill=black]

\begin{subfigure}[b]{0.5\textwidth}
\begin{center}
\begin{tikzpicture}[scale=1.15, every node/.style={transform shape}]
\tikzstyle{state}=[shape=circle,draw=black,fill=black!8,minimum size=32pt]
\tikzstyle{observation}=[shape=circle,draw=black,fill=black!8,minimum size=32pt]
\node[state] (s1) at (0,2) {$A$};
\node[state] (s2) at (2,3) {$B$}
    edge [mainedge] node[auto,swap] {} (s1);
\node[state] (s3) at (4,1) {$C$}
    edge [mainedge] node[auto,swap] {} (s2);
\node[observation] (x1) at (1,-1) {$D$}
    edge [mainedge] node[left] {} (s1)
    edge [mainedge] node[left] {} (s3);
\node[observation] (x2) at (2,1) {$E$}
    edge [mainedge] node[left] {} (s2);
\end{tikzpicture}
\caption{Bayesova mreža}
\label{bayesnet}
\end{center}
\end{subfigure}
\begin{subfigure}[b]{0.5\textwidth}
\begin{center}
\begin{tikzpicture}[scale=1.15, every node/.style={transform shape}]
\tikzstyle{state}=[shape=circle,draw=black,fill=black!8,minimum size=32pt]
\tikzstyle{observation}=[shape=circle,draw=black,fill=black!8,minimum size=32pt]
\node[state] (s1) at (0,2) {$A$};
\node[state] (s2) at (2,3) {$B$}
    edge [thick] node {} (s1);
\node[state] (s3) at (4,2) {$C$}
    edge [thick] node {} (s2);
\node[observation] (x1) at (0,0) {$D$}
    edge [thick] node {} (s1);
\node[observation] (x2) at (2,1) {$E$}
    edge [thick] node {} (s2)
    edge [thick] node {} (s1);
\node[observation] (x3) at (3,0) {$F$}
    edge [thick] node {} (s3);
\end{tikzpicture}
\caption{Markovljeva mreža}
\label{markovnet}
\end{center}
\end{subfigure}
\caption{Prikaz predložaka grafičkih modela.}
\label{bayesmarkovnetfig}
\end{figure}

\section{Motivacijski primjer}
Primjer je preuzet iz skupa uvodnih predavanja tečaja na Courseri \cite{ProbGraphModCoursera}.

Na slici \ref{ilustrativniprimjer1} možemo vidjeti ilustrativni primjer jednostavne Bayesove mreže. Zavisnosti i nezavisnosti između pojedinačnih varijabli su složene tako da onome koji ih je slagao imaju smisla. Pretpostavka je da težina predmeta i kvocijent inteligencije studenta utječu na ocjenu koju će student dobiti. Također, inteligencija utječe i na rezultate državne mature, a samo ocjena smije utjecati na pismo preporuke koje će profesor napisati.

Zgodno je postaviti opće pitanje -- kako će opažanje slučajne varijable $X$ utjecati na slučajnu varijablu $Y$? Postoji nekoliko varijanti koje su nabrojane ispod:
\begin{enumerate}
\item $X \rightarrow Y$
\item $X \leftarrow Y$
\item $X \rightarrow W \rightarrow Y$
\item $X \leftarrow W \leftarrow Y$
\item $X \leftarrow W \rightarrow Y$
\item $X \rightarrow W \leftarrow Y$
\end{enumerate}

Možemo iz primjera dati odgovore. Opažanjem jako visokog kvocijenta inteligencije sigurno možemo reći da će to utjecati na vrijednosti koje možemo očekivati za rezultat državne mature. Opažanjem jako visokog rezultata mature možemo nešto reći i o samom kvocijentu inteligencije studenta. Za treću varijantu možemo kao primjer uzeti opažanje jako niske inteligencije studenta, samim time znamo da će ocjena biti niža pa će zbog toga i vjerojatnost dobivanja pisma preporuke opasti. Slično je razmišljanje i kod 4.~slučaja. Slučaj 5.~se može odnositi na \textbf{$\text{Ocjena} \leftarrow \text{IQ} \rightarrow \text{Matura}$}. Ako opazimo jako visoku ocjenu očito je da možemo zaključiti da je kvocijent inteligencije viši (ili je predmet jako lak), samim time i rezultat mature bi morao biti bolji. Vrijedilo bi čak i suprotno, opažanjem rezultata na maturi (pretpostavljajući da je matura dovoljno teška) možemo vrlo jednostavno zaključiti da će vrijednost slučajne varijable ocjene morati biti puno drugačija s obzirom na novo znanje. Šesti slučaj je jedini na koji ćemo odgovoriti negativno. Opažanje težine predmeta ne bi trebalo utjecati na kvocijent inteligencije, bar ne u ovako definiranim zavisnostima. Isto vrijedi i za opažanje kvocijenta inteligencije. Pretpostavljajući da svi studenti prate sličan ili isti kurikulum, s povećanjem kvocijenta inteligencije objektivna težina predmeta ne bi trebala biti pod utjecajem tog opažanja. Jednostavniji primjer za shvatiti je $X + Y = W$ prikazan na slici \ref{ilustrativniprimjer2}. Slučajne varijable $X$ i $Y$ mogu poprimiti vrijednosti $0$ i $1$, što znači da varijabla $W$ može poprimiti vrijednosti iz skupa $\{0,1,2\}$. Opažanjem $X$ broj mogućih vrijednosti $Y$ se uopće neće promijeniti dokle god nema novih informacija o $W$, isto vrijedi i za opažanje varijable $Y$ i utjecaj toga na $X$. Opažanjem varijable $W$ očito je iz drugog slučaja da će to opažanje utjecati na moguće vrijednosti i $X$ i $Y$.

Iz ova dva primjera može se zaključiti kako razmišljanje o vjerojatnosnim modelima koji su predstavljeni grafom može biti vrlo intuitivno. Puno lakše nego da smo simboličkim raspisivanjem formule distribucije koristeći Bayesov teorem pokušavali doći do istih zaključaka -- naravno, rezultat ta dva postupka bi na kraju trebao biti potpuno isti.

Postoji još mnoštvo drugih jednostavnih opažanja koja nam omogućuju generalniji pristup vjerojatnosnim grafičkim modelima i mnoštvo današnjih metoda se bazira na njima. Optimalne i aproksimativne algoritme učenja i zaključivanja moguće je generalizirati na bilo kakve oblike grafova upravo zbog opažanja koja su slična ovima u navedenim primjerima.

Rad s Markovljevim mrežama je malo drugačiji te se čitatelj upućuje na tečaj i literaturu \cite{ProbGraphModCoursera, koller2009probabilistic, wainwright2008graphical}.

\begin{figure}[H]
\begin{subfigure}[b]{0.45\textwidth}
\begin{center}
\begin{tikzpicture}[scale=1.15, every node/.style={transform shape}]
\tikzstyle{lightedge}=[<-,dotted]
\tikzstyle{mainstate}=[state,thick]
\tikzstyle{mainedge}=[<-,thick]
\tikzstyle{undirect}=[draw=black,fill=black]
\tikzstyle{state}=[shape=circle,draw=black,fill=black!8,minimum size=32pt]
\tikzstyle{observation}=[shape=circle,draw=black,fill=black!8,minimum size=32pt]
\node[state] (s1) at (0,2) {Težina};
\node[state] (s2) at (2,2) {IQ};
\node[state] (s3) at (4,1) {Matura}
    edge [mainedge] node[auto,swap] {} (s2);
\node[observation] (s4) at (1,0) {Ocjena}
    edge [mainedge] node[left] {} (s2)
    edge [mainedge] node[auto,swap] {} (s1);
\node[observation] (s5) at (1,-2) {Pismo}
    edge [mainedge] node[left] {} (s4);
\end{tikzpicture}
\end{center}
\caption{Student i predmet koji pohađa.}
\label{ilustrativniprimjer1}
\end{subfigure}
\begin{subfigure}[b]{0.45\textwidth}
\begin{center}
\begin{tikzpicture}[scale=1.15, every node/.style={transform shape}]
\tikzstyle{lightedge}=[<-,dotted]
\tikzstyle{mainstate}=[state,thick]
\tikzstyle{mainedge}=[<-,thick]
\tikzstyle{undirect}=[draw=black,fill=black]
\tikzstyle{state}=[shape=circle,draw=black,fill=black!8,minimum size=32pt]
\tikzstyle{observation}=[shape=circle,draw=black,fill=black!8,minimum size=32pt]
\node[state] (s1) at (0,2) {$X$};
\node[state] (s2) at (2,2) {$Y$};
\node[state] (s3) at (1,0) {$W$}
    edge [mainedge] node[auto,swap] {} (s1)
    edge [mainedge] node[auto,swap] {} (s2);    
\end{tikzpicture}
\end{center}
\caption{$X + Y = W$}
\label{ilustrativniprimjer2}
\end{subfigure}
\caption{Ilustrativni primjeri Bayesovih mreža. Na slici \ref{ilustrativniprimjer1} mreža predstavlja slučajne varijable i njihove međuovisnosti. Pitanje je kolika je vjerojatnost da student dobije pismo preporuke -- s obzirom na sve ostale vrijednosti -- na nekom određenom predmetu (uzimajući u obzir da karakteristike profesora ne utječu na ključnu varijablu -- potpuna objektivnost). Na slici \ref{ilustrativniprimjer2} -- mreža predstavlja slučajne varijable $X$ i $Y$ koje mogu poprimiti vrijednosti $0$ ili $1$ i izraz $X + Y = W$. Ovaj zbroj može se predočiti tom mrežom.}
\end{figure}

\section{Prednosti i mane}

Prednost vjerojatnosnih grafičkih modela je njihova kompaktnost. Sve zavisnosti i nezavisnosti mogu se predstaviti s vrlo malo komponenata i grafički model je zbog toga vrlo memorijski efikasan. S druge strane, najsuvremeniji modeli dubokih neuronskih mreža zahtijevaju ogromne količine memorije, a za njihovo treniranje potrebne su grafičke procesne jedinice (engl.~\emph{graphical processing unit -- GPU}) izvanprosječnih specifikacija.

Tako velika ekspresivnost u tako kompaktnoj reprezentaciji zna ponekad biti i previše. Ako stvarno ne pazimo, onda je moguće složiti takav model za koji ne postoji efikasan optimalni algoritam učenja ili čak zaključivanja. U tom slučaju koriste se aproksimativni algoritmi, a oni široke primjene ovise o pseudoslučajnim brojevima. U tom će slučaju evaluacija i podešavanje modela uzimati jako puno vremena. Također, ako nismo sigurni kakva treba biti reprezentacija modela, onda je moguće da ćemo izgubiti puno vremena slagajući strukturu grafa koja će imati dobre performanse na našim podacima.

Jedan od glavnih razloga zašto su vjerojatnosni grafički modeli široko prisutni u analizi sentimenta je taj što su oni danas jedini mogući okvir za predviđanje strukture (engl.~\emph{structured prediction}) ili učenje elemenata u kojima je prisutna određena struktura (engl.~\emph{structured output learning}). 

Predviđanje strukture je generalizacija standardnih pristupa nadziranog učenja -- klasifikacije i regresije. Pristupi uče modele minimiziranjem funkcije pogreške dobivanjem što sličnijih izlaza onima na skupu za učenje. Kod klasifikacije izlaz (ili domena izlaza) je diskretna oznaka razreda, a kod regresije je skup realnih brojeva. Kod predviđanja strukture domena izlaza, a i funkcija pogreške mogu biti u potpunosti proizvoljne. Onda je moguće da cilj nije razred ili broj nego neka komplicirana struktura poput niza ili grafa.

Postoji mnogo drugih pristupa poput strukturnog stroja potpornih vektora, strukturni perceptron i sl., ali neke kompliciranije strukture poput stabala ili skip-lista je teže enkodirati pomoću tih modela. Čak je i u području dubokog učenja konsenzus taj da je još uvijek jedan od neriješenih problema predviđanje strukture. U većini slučajeva bi se modeli dubokog učenja povezali s uvjetnim slučajnim poljima i drugim vjerojatnosnim grafičkim modelima \cite{yann2015what-cvpr}.

\chapter{Pregled pristupa}\label{poglavlje3}
Pokušala se pokriti za svaki od pristupa opisanih u odjeljku \ref{razinegranularnosti}, analiza sentimenta vezana uz svaku razinu.

Vjerojatnosni grafički modeli koji se koriste u analizi sentimenta su pretežito tematski modeli (engl.~\emph{topic models}) i modeli bazirani na učenju nizova (engl.~\emph{sequential learning}) poput skrivenih Markovljevih modela (engl.~\emph{Hidden Markov Models}) i uvjetnih slučajnih polja (engl.~\emph{Conditional Random Fields}). 

\section{Analiza sentimenta kao niz pojedinačnih sentimenta}

\citet{jin2009novel} su primijenili HMM za izvlačenje aspekata i izraza koji definiraju neko mišljenje. \citet{jakob2010extracting, breck2007identifying} su za to koristili CRF. Za združeno klasificiranje sentimenta rečenice i dokumenta (moguće je i paragrafa, a i aspekata) koristio se prilagođeni CRF s više razina u \cite{mcdonald2007structured} (prikaz \ref{crfsentpardoc}). \citet{choi2005identifying} su koristili CRF za identifikaciju izvora mišljenja tj. pokušao se otkriti entitet koji ima mišljenje o nekom drugom entitetu.

Za razliku od originalnog CRFa, koji može koristiti samo nizove za učenje, Skip-CRF i Tree-CRF omogućuju običnom modelu da iskorištava strukturne značajke i takav model koristio se u \cite{li2010structure}. Zadatak je bio sažeti skup recenzija vezanih uz proizvod, uslugu, lik, film, redatelja. Tražio se objekt, mišljenje recenzije (npr.~\emph{bateriju je lako koristiti}) i polaritet tog mišljenja. Problem je pretvoren u označavanje niza riječi u recenziji i koristi se jednostavan BIO (engl.~\emph{beginning, inside, other}) skup oznaka, a za izgradnju Skip-CRFa i Tree-CRFa korištene su značajke poput oznake vrste riječi, semantičkog leksikona pomoću kojih se prije algoritma zaključivanja izgrađivalo stablo i skip čvorovi. Sama struktura novih modela zahtijevala je nestandardne aproksimativne algoritme zaključivanja \cite{wainwright2003tree}. Skip-CRF je u tom radu imao bolje performanse od spomenutog HMM modela u \cite{jin2009novel}.

\begin{figure}[H]
\begin{subfigure}[b]{0.45\textwidth}
\begin{center}
		    \centering
		    \def\svgwidth{\columnwidth}
		    \resizebox{0.5\textwidth}{!}{\input{markovblanket.pdf_tex}}
\end{center}
\caption{Markovljev pokrivač.}
\label{markovblanket}
\end{subfigure}
\begin{subfigure}[b]{0.45\textwidth}
\begin{center}
\begin{tikzpicture}[scale=0.15, every node/.style={transform shape}]
\tikzstyle{lightedge}=[<-,dotted]
\tikzstyle{mainstate}=[state,thick]
\tikzstyle{mainedge}=[<-,thick]
\tikzstyle{undirect}=[draw=black,fill=black]
\tikzstyle{state}=[shape=circle,draw=black,fill=black!8,minimum size=32pt]
\tikzstyle{observation}=[shape=circle,draw=black,fill=black!50,minimum size=32pt]
% observed sentences
\foreach \x in {4,3,2,1}
    \node[observation] (x-\x) at (-\x*3,0) {$x_{i-\x}$};

\node[observation] (x0) at (0,0) {$x_{i}$};

\foreach \x in {1,2,3,4}
    \node[observation] (x\x) at (\x*3,0) {$y_{i+\x}$};

% sentiment of each sentence
\foreach \x in {4,3,2,1}
    \node[state] (y-\x) at (-\x*3,5) {$y_{i-\x}$};

\node[state] (y0) at (0,5) {$x_{i}$};

\foreach \x in {1,2,3,4}
    \node[state] (y\x) at (\x*3,5) {$y_{i+\x}$};

% edges between sentences and sentiment
\foreach \x in {4,3,2,1}
   \draw (x\x) -- (y\x);

\draw (x0) -- (y0);

\foreach \x in {4,3,2,1}
   \draw (x-\x) -- (y-\x);
   
% paragraphs
\foreach \x in {-1,0,1}
    \node[state] (p\x) at (\x*9,10) {$p_{i\x}$};
% edges to paragraphs

\foreach \x in {-4,-3,-2}
   	\draw (y\x) -- (p-1);
\foreach \x in {-1,0,1}
   	\draw (y\x) -- (p0);
\foreach \x in {2,3,4}
   	\draw (y\x) -- (p1);
   	
% edges between sentence sentiment variables
\foreach \x [evaluate=\x as \ystart using int(\x+1)] in {-4, ..., 3} {
	\draw (y\x) -- (y\ystart);
}
% edges between paragraph sentiment variables
\foreach \x [evaluate=\x as \ystart using int(\x+1)] in {-1, 0} {
	\draw (p\x) -- (p\ystart);
}
% document sentiment
\node[state] (d0) at (0,15) {$d_{i}$};
\foreach \x in {-1,0,1}
   	\draw (p\x) -- (d0);
\end{tikzpicture}
\end{center}
\caption{Model uvjetnih slučajnih polja koji združeno modelira sentiment za rečenice, paragrafe i dokument.}
\label{crfsentpardoc}
\end{subfigure}
\caption{Prikaz nekih od korištenih modela u citiranim radovima.}
\end{figure}

\section{Tematski modeli}

Tematski modeli su uvedeni za istovremenu analizu tema i sentimenta u dokumentu. Takvi pristupi gdje se zajedno modelira tema i sentiment imaju prednost jer iskorištavaju vezu između teme i sentimenta, i pokazano je da bolje rade od tradicionalnih pristupa. Nažalost, velik dio tematskih modela pretpostavlja da su, za dane parametre, svi sentimenti riječi u dokumentu nezavisni. \citet{li2010sentiment} su na osnovni tematski model LDA (engl.~\emph{latent Dirichlet analysis}) dodali sentiment i međuovisnosti između riječi i sentimenta. Za algoritam zaključivanja se koristilo Gibbsovo uzorkovanje, ali je bilo potrebno izvesti formule uvjetne razdiobe kako bi nadograđeni model bio pravilno definiran. Za točan generativni proces \emph{plate} modela čitatelj se upućuje na rad.
\citet{titov2008modeling} su pokazali da globalni tematski modeli poput LDA nisu prikladni za otkrivanje aspekata. Razlog tome je da LDA za identificiranje tema i vjerojatnosne distribucije riječi u svakoj temi ovisi o razlikama u distribuciji tema i zajedničkim pojavljivanjima riječi u dokumentima. Dokumenti koji u sebi sadržavaju mišljenje o nekom proizvodu su poprilično homogeni što znači da svaki dokument govori o istim aspektima. Ta činjenica čini globalne tematske modele nedovoljno učinkovitima za otkrivanje aspekata i jedino im je lako pronaći entitete o kojima se govori. Autori su onda predložili višerazinski tematski model. Globalni model otkriva entitete dok lokalni modeli koriste par rečenica (ili klizeći prozor) kao dokument. U tom slučaju je svaki aspekt modeliran unigramskim jezičnim modelom. Različite riječi koje se koriste uz isti ili povezani predmet mišljenja u sličnom (pozitivnom ili negativnom) kontekstu će biti grupirane pod isti aspekt. Nažalost, ova tehnika ne odvaja aspekte i riječi vezane uz sentiment.

Unatoč tome što su tematski modeli već dovoljno formalno analizirani i mogu modelirati mnogo tipova informacije, postoje nedostaci kojima se ograničava praktično korištenje tih modela u stvarnom životu. Jedan od glavnih razloga je što je potrebna ogromna količina podataka i ogromna količina vremena za podešavanje parametara kako bi se postigli zadovoljavajući rezultati. Stvari su još gore jer većina tematskih modela koristi metode poput Gibbsovog uzorkovanja, koje daje malo drugačije rezultate nakon svakog pojedinačnog pokretanja zbog MCMC (engl.~\emph{Markov chain Monte Carlo}) uzorkovanja, a samo to čini podešavanje parametara vremenski zahtjevnim.

\section{Ostalo}

\citet{airoldi2006markov, bai2011predicting} koriste vjerojatnosni grafički model u obliku Markovljevog pokrivača -- slika \ref{markovblanket} -- te tabu pretragom otkrivaju najbolje zavisnosti distribucije sentimenta u ovisnosti o riječima. Pretragom se neke zavisnosti i eliminiraju pa se od početnog rječnika od nekoliko tisuća riječi model svede na samo tridesetak.

U \cite{fang2012mining, lerman2009contrastive, paul2010summarizing} mogu se vidjeti razni pristupi problemu koji je usko vezan uz analizu sentimenta -- modeliranje oprečnih mišljenja. Pristupi navedeni pokušavaju na različite načine izlučiti ili sažeti oprečna mišljenja. Problem je za danu temu dati mišljenja koja imaju što veću razliku u polaritetu. \citet{fang2012mining} proširuju svoj model dodajući mogućnost odabira entiteta koji imaju mišljenje, a cilj je za dana dva entiteta dati njihovo neslaganje vezano uz danu temu. Svi pristupi intenzivno koriste tematske modele.

\chapter{Zaključak}

Iz ovog seminarskog rada može se zaključiti da su vjerojatnosni modeli vrlo učinkoviti u rješavanju problema analize sentimenta i mišljenja.
Unatoč tome što nije jednostavno koristiti aproksimativne algoritme za složenije modele te što je i sam odabir zavisnosti između opažanih varijabli fiksan i nepromijenjiv, a u nekim slučajevima i vremenski zahtjevan, dan pregled pokazuje da navedeni nedostaci vjerojatnosnih grafičkih modela ne utječu na rezultate koji su više i nego zadovoljavajući. \citet{liu2012sentiment} u svojoj zaključnoj riječi predviđa da će cijeli problem biti riješen na zadovoljavajućoj razini u bliskoj budućnosti za široki spektar primjena. Trenutno, za pravu primjenu, potpuno automatizirano i precizno rješenje ne postoji. 

Vjerojatnosni grafički modeli su trenutno jedini dovoljno jak pristup strukturnom predviđanju, ali bi u sljedećih nekoliko godina istraživači koji rade na dubokom učenju mogli probiti i taj zid. \citet{zhang2015text} su pokazali da je moguće na jako velikim skupovima za učenje postići rezultate koji su vrlo blizu 100\% za prepoznavanje sentimenta koristeći samo niz znakova iz dokumenta. Njihova duboka konvolucijska mreža naučila je sama koncept riječi, rečenice, paragrafa, a i još mnogo toga. Ovako nešto vrlo vjerojatno ne bi mogli izvesti vjerojatnosnim grafičkim modelima, ali također, ovako nešto ne bi mogli izvesti bez jako dobrih grafičkih procesnih jedinica. Možemo samo nagađati koliko dugo je trajalo podešavanje dubine mreže, podešavanje parametara, regularizacija i testiranje.

U zadnjih nekoliko godina pojava vjerojatnosnih programskih jezika (engl.~\emph{probabilistic programming language}) bi mogla znatno utjecati na popularnost vjerojatnosnih grafičkih modela. Zbog svojstva općenitosti vjerojatnosnih grafičkih modela moguće je vrlo jednostavno izraziti grafičke modele i automatski dobiti efikasan aproksimativni algoritam učenja i zaključivanja. Ovo omogućuje vrlo lako izražavanje preciznih međuovisnosti između opažanih varijabli koje su potrebne za modeliranje sentimenta. Vjerojatno će zbog toga u budućnosti broj tako definiranih modela narasti, a analiza sentimenta će se samim time poboljšati.

\bibliography{literatura}
\bibliographystyle{fer}

\chapter{Sažetak}
U ovom seminarskom radu proučen je problem analize sentimenta i mišljenja. Područje primjene koje problem obuhvaća je veliko, a uspješna integracija analize sentimenta u postojeće sustave bi uvelike mogla poboljšati kvalitetu korisnika ili situacije u društvu (ako je sustav primijenjen na političke i socijalne tekstove).

Definirale su se razine na kojoj možemo vršiti analizu sentimenta -- izrazi mišljenja, rečenice, paragrafi i dokument. Neki problemi unutar obrade prirodnog jezika -- kao što su razrješavanje koreferenci, obrada negacije i sl. -- nisu u potpunosti riješeni stoga bi analiza sentimenta u budućnosti mogla biti puno bolja.

U seminarskom radu dan je kratak uvod u vjerojatnosne grafičke modele u kojima su dani primjeri koji opisuju zašto je intuitivnije razmišljati o vjerojatnosnim modelima u obliku grafa. Izdvojene su prednosti i mane vjerojatnosnih grafičkih modela. Zaključak je taj da su vjerojatnosni grafički modeli danas jedan od najboljih izbora za analizu sentimenta -- jedini modeli koji omogućuju da se u potpunosti iskorištava struktura u tekstu.

Pregled pristupa obradio je sve razine na kojima možemo vršiti analizu sentimenta. Može se primijetiti da su jedni od najboljih i glavnih pristupa bili tematski modeli i modeliranje niza.

\end{document}
